@extends('layouts.app')
@section('title','Login')
@section('content')
    <div class="container mt-5 mb-5">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="card px-5 py-5">
                    <main class="form-signin">                        
                        <form id="loginForm" class="needs-validation" name="loginForm" action="../../controllers/login_form.php" method="POST">
                            <div class="form-reg">
                                <div class="form-group">  
                                    <input type="text"  class="form-control my-1" id="emailLog" name="email" placeholder="Login/email" value="<?php echo isset($emailc) ? $emailc : "";?>"/><br>
                                    <div class='text-danger' id="emailLogError" style="display: none;">Email can`t be empty</div>
                                    <div class='text-danger' id="emailLoginPhpError"><?php echo isset($phpLoginErrors['logEmail']) ? $phpLoginErrors['logEmail'] : ""; ?></div>
                                </div>
                            </div>

                            <div class="form-reg">
                                <div class="form-group">  
                                    <input type="password"  class="form-control my-1" id="passLog" name="password" placeholder="password" /><br>
                                    <div class='text-danger' id="passLogError" style="display: none;">Password can`t be empty</div>
                                    <div class='text-danger' id="passLoginPhpError"><?php echo isset($phpLoginErrors['logPass']) ? $phpLoginErrors['logPass'] : ""; ?></div>                                    
                                </div>
                            </div>

                            <input id="submitLogin" type="submit" value="login" class="btn btn-outline-primary text-uppercase" name="login">
                            <div class="form-group">
                                <div class="form-control my-5">
                                    Back to <a href="registration">Registration</a>
                                </div>
                            </div>  
                        </form>                        
                    </main>
                </div>
            </div>
        </div>    
    </div>
@endsection
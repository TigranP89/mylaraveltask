@extends('layouts.app')
@section('title','Product')
@section('content')
    <nav class="navbar navbar-expand navbar-dark bg-dark" aria-label="Second navbar example">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarsExample02">
                <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="user_list">Home</a>
                </li>                
                </ul>

                <div>
                    <form action="../../controllers/logout_form.php" method="POST">
                        <button class="btn btn-danger float-end m-3">LOGOUT</button>	
                    </form>    
                </div>            
            </div>
        </div>
    </nav>
    
    <div class="jumbotron jumbotron-fluid">
        <div class="container">                
            <form action="addNewProduct" method="POST">
                <table class="table">
                    <tr aling="center">
                        <th colspan="2"><h1>Add New Product</h1></th>
                    </tr>
                    
                    <tr>
                        <th>Product name</th><td><input class="form-control" type="text" name="itemName" placeholder="Item name"> </td> 
                    </tr>
                    <tr>
                        <th>Description</th><td><textarea class="form-control" name="description" aria-label="With textarea" rows="10" style="height:100%;" placeholder="Description"></textarea></td> 
                    </tr>
                    <tr>
                        <th>Price,$</th><td><input class="form-control" type="text" name="price" placeholder="Price"> </td> 
                    </tr>
                </table>
                <div>	
                    <button class="btn btn-outline-success">Add Product</button>	
                </div>
            </form>  
        </div>                
    </div>
@endsection
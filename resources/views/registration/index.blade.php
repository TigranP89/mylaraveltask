@extends('layouts.app')
@section('title','Registaration')
@section('content')
    <div class="container mt-5 mb-5">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="card px-5 py-5">
                    <main class="form-signin">                                 
                        <form id="registrationForm" class="needs-validation" name="registrationForm" action="{{route('registrationForm')}}" method="POST">
                         @csrf
                            <div class="form-reg">                               
                                <div class="form-group">
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="">
                                    @if ($errors->has('fname'))
                                    <div class='text-danger' id="fnamePhpError">@error('fname') {{$message}} @enderror</div>
                                    @endif
                                    <br><br>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="">
                                    @if ($errors->has('lname'))
                                    <div class='text-danger' id="fnamePhpError">@error('lname') {{$message}} @enderror</div>
                                    @endif
                                    <br><br>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="">
                                    @if ($errors->has('email'))
                                    <div class='text-danger' id="fnamePhpError">@error('email') {{$message}} @enderror</div>
                                    @endif
                                    <br><br>
                                </div>
                                <div class="form-group">            
                                    <input type="password" class="form-control" id="pass" name="password" placeholder="Password">
                                    @if ($errors->has('password'))
                                    <div class='text-danger' id="fnamePhpError">@error('password') {{$message}} @enderror</div>
                                    @endif
                                    <br><br>
                                </div>
                                <div class="form-group">           
                                    <input type="password" class="form-control" id="repass" name="repass" placeholder="Repeat password">
                                    @if ($errors->has('repass'))
                                    <div class='text-danger' id="fnamePhpError">@error('repass') {{$message}} @enderror</div>
                                    @endif
                                    <br><br>
                                </div>
                                <input id="submitInput" type="submit" value="Submit" class="btn btn-outline-success text-uppercase" name="submit">
                                <div class="form-group">           
                                    <div class="form-control my-5">
                                        Already registered ? Go to <a href="login">Login</a>
                                    </div>
                                </div>
                            </div>                        
                        </form>                    
                    </main>
                </div>
            </div>
        </div>    
    </div>    
@endsection
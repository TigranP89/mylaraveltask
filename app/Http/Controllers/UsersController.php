<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class UsersController extends Controller
{
    public function index(){
        return view('registration.index');
    }
    public function login(){
        return view('login.index');
    }
    public function registrationForm(Request $request){
        $request->validate([
            'fname' => ['required'],
            'lname' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required', Password::min(8)
                ->mixedCase()
                ->letters()
                ->numbers()
                ->symbols()
                ->uncompromised(),],
            'repass' => ['required', 'same:password'],
        ]);

    }
}
